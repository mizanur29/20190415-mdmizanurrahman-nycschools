package com.nycschools.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Score implements Comparable<Score>{

    @SerializedName("dbn")
    @Expose
    private String schoolDBN;

    @SerializedName("school_name")
    @Expose
    private String schoolName;

    @SerializedName("num_of_sat_test_takers")
    @Expose
    private String numOfApplicant;

    @SerializedName("sat_math_avg_score")
    @Expose
    private String mathScore;

    @SerializedName("sat_writing_avg_score")
    @Expose
    private String writingScore;

    @SerializedName("sat_critical_reading_avg_score")
    @Expose
    private String readingScore;

    public String getSchoolDBN() {
        return schoolDBN;
    }

    public void setSchoolDBN(String schoolDBN) {
        this.schoolDBN = schoolDBN;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public String getNumOfApplicant() {
        return numOfApplicant;
    }

    public void setNumOfApplicant(String numOfApplicant) {
        this.numOfApplicant = numOfApplicant;
    }

    public String getMathScore() {
        return mathScore;
    }

    public void setMathScore(String mathScore) {
        this.mathScore = mathScore;
    }

    public String getWritingScore() {
        return writingScore;
    }

    public void setWritingScore(String writingScore) {
        this.writingScore = writingScore;
    }

    public String getReadingScore() {
        return readingScore;
    }

    public void setReadingScore(String readingScore) {
        this.readingScore = readingScore;
    }

    @Override
    public int compareTo(Score o) {
        return schoolDBN.compareToIgnoreCase(o.schoolDBN);
    }
}