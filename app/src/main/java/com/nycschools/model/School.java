package com.nycschools.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class School implements Comparable<School>{

    @SerializedName("dbn")
    @Expose
    private String schoolDBN;

    @SerializedName("school_name")
    @Expose
    private String name;

    @SerializedName("school_email")
    @Expose
    private String email;

    @SerializedName("primary_address_line_1")
    @Expose
    private String address;

    @SerializedName("city")
    @Expose
    private String city;

    @SerializedName("zip")
    @Expose
    private String zipCode;

    public String getSchoolDBN() {
        return schoolDBN;
    }

    public void setSchoolDBN(String schoolDBN) {
        this.schoolDBN = schoolDBN;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    @Override
    public int compareTo(School o) {
        return schoolDBN.compareToIgnoreCase(o.schoolDBN);
    }
}