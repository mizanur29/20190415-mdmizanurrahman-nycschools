package com.nycschools.utility;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.view.WindowManager;

public class Utility {

    public static AlertDialog alertDialog;

    public static AlertDialog alertOK(Activity activity, String message, Boolean systemAlertDialog) {
        alertDialog = null;
        if (!isActivityDestroyed(activity)) {
            AlertDialog.Builder builder = new AlertDialog.Builder(activity);
            builder.setMessage(message)
                    .setCancelable(false)
                    .setPositiveButton(activity.getResources().getString(android.R.string.ok),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                }
                            });

            alertDialog = builder.create();

            if (systemAlertDialog) {
                alertDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
            }

            alertDialog.show();
            alertDialog.getButton(DialogInterface.BUTTON_POSITIVE).setTextColor(Color.BLACK);
        }
        return alertDialog;
    }

    public static boolean isActivityDestroyed(Activity activity) {
        if (activity != null) {
            return (activity.isDestroyed());
        } else {
            return true;
        }
    }
}
