package com.nycschools.utility;

/**
 * A simple Constants class.
 * Contains all constants to use through the application.
 */
public class Constants {

    public static final String BASE_URL = "https://data.cityofnewyork.us/resource/";
}
