package com.nycschools.utility;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;


/**
 * A simple FragmentController class.
 * make all fragment operations
 */
public class FragmentController {

    /**
     * Change the Fragment using container ID
     * store fragment to the back stack
     */
    public static void changeFragment(Fragment fragment,int containerId ,FragmentManager fragmentManager){
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.addToBackStack(fragment.getClass().getSimpleName());
        transaction.replace(containerId,fragment);
        transaction.commit();
    }
}
