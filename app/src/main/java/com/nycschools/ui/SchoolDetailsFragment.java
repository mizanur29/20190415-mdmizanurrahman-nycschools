package com.nycschools.ui;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.nycschools.MyApplication;
import com.nycschools.R;
import com.nycschools.model.School;
import com.nycschools.model.Score;
import com.nycschools.network.APIClient;
import com.nycschools.network.services.SchoolAPI;
import com.nycschools.utility.Utility;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link SchoolDetailsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SchoolDetailsFragment extends Fragment {
    private Activity activity;
    protected MyApplication myApplication;
    private ProgressBar mProgressBar;
    private LinearLayout satDetailsLayout;
    protected SchoolAPI schoolAPI;
    private School school;
    private TextView title;
    private FrameLayout backArrow;
    private TextView txtDBN;
    private TextView txtSchoolName;
    private TextView txtAddress;
    private TextView txtEmail;
    private TextView txtNumberOfApplicants;
    private TextView txtReadingScore;
    private TextView txtMathScore;
    private TextView txtWritingScore;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param school Parameter 1.
     * @return A new instance of fragment SchoolDetailsFragment.
     */
    public static SchoolDetailsFragment newInstance(School school) {
        SchoolDetailsFragment fragment = new SchoolDetailsFragment();
        fragment.setSchool(school);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.activity = this.getActivity();
        myApplication = MyApplication.getInstance();
        schoolAPI = APIClient.getClient().create(SchoolAPI.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_school_details, container, false);
    }

    /**
     * Initialize View component
     * after view has been created
     */
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        title = view.findViewById(R.id.txt_title);
        backArrow = view.findViewById(R.id.layout_back_button);
        txtDBN = view.findViewById(R.id.txt_dbn);
        txtSchoolName = view.findViewById(R.id.txt_school_name);
        txtAddress = view.findViewById(R.id.txt_address);
        txtEmail = view.findViewById(R.id.txt_email);
        txtNumberOfApplicants = view.findViewById(R.id.txt_num_of_applicants);
        txtReadingScore = view.findViewById(R.id.txt_reading_score);
        txtMathScore = view.findViewById(R.id.txt_math_score);
        txtWritingScore = view.findViewById(R.id.txt_writing_score);
        mProgressBar = view.findViewById(R.id.progressBar);
        satDetailsLayout = view.findViewById(R.id.layout_sat_details);
    }

    @Override
    public void onResume() {
        super.onResume();
        setActionBar();
        if (myApplication == null || myApplication.getScores() == null || myApplication.getScores().size() == 0) {
            getScore();
        } else {
            setScore();
        }
    }

    /**
     * Set ActionBar data and action for Back button
     */
    private void setActionBar() {
        if(title !=null && backArrow != null) {
            title.setText(getResources().getString(R.string.sat_title));
            backArrow.setVisibility(View.VISIBLE);
            backArrow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    activity.onBackPressed();
                }
            });
        }
    }

    /**
     * Make a network request to the Score list API
     * After getting successful response set Score list to the MyApplication class
     */
    private void getScore() {
        if (myApplication.getNetworkUtility().isNetworkAvailable(this.getActivity())) {
            if(mProgressBar !=null && satDetailsLayout != null){
                mProgressBar.setVisibility(View.VISIBLE);
                satDetailsLayout.setVisibility(View.GONE);
            }
            Call<List<Score>> scoreCall = schoolAPI.getScores();
            scoreCall.enqueue(new Callback<List<Score>>() {
                @Override
                public void onResponse(Call<List<Score>> call, Response<List<Score>> response) {
                    if (response.body() != null) {
                        myApplication.setScores(response.body());
                        mProgressBar.setVisibility(View.GONE);
                        satDetailsLayout.setVisibility(View.VISIBLE);
                        setScore();
                    }
                }

                @Override
                public void onFailure(Call<List<Score>> call, Throwable t) {

                }
            });
        } else {
            Utility.alertOK(this.getActivity(), "Network Unavailable", false);
        }
    }

    /**
     * Set Score details to the view for specific School
     */
    protected void setScore() {
        if (myApplication.getScores() != null && myApplication.getScores().size() > 0) {
            for (Score s : myApplication.getScores()) {
                if (school.getSchoolDBN() != null) {
                    if (s.getSchoolDBN().toLowerCase().equals(school.getSchoolDBN().toLowerCase())) {
                        txtDBN.setText(school.getSchoolDBN());
                        txtSchoolName.setText(school.getName());
                        txtEmail.setText(school.getEmail());
                        txtAddress.setText(school.getAddress() + ", " + school.getCity() + ", " + school.getZipCode());
                        txtNumberOfApplicants.setText(s.getNumOfApplicant());
                        txtReadingScore.setText(s.getReadingScore());
                        txtMathScore.setText(s.getMathScore());
                        txtWritingScore.setText(s.getWritingScore());
                        break;
                    }
                }
            }
        }
    }

    public void setSchool(School school) {
        this.school = school;
    }

    public School getSchool() {
        return school;
    }
}