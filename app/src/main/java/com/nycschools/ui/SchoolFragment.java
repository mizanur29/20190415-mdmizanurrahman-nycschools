package com.nycschools.ui;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.nycschools.MyApplication;
import com.nycschools.R;
import com.nycschools.adapter.SchoolAdapter;
import com.nycschools.model.School;
import com.nycschools.network.APIClient;
import com.nycschools.network.services.SchoolAPI;
import com.nycschools.utility.Utility;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 * create an instance of this fragment.
 */
public class SchoolFragment extends Fragment {

    protected View view;
    protected MyApplication myApplication;
    private RecyclerView recyclerView;
    private TextView title;
    private ProgressBar mProgressBar;
    private SchoolAPI schoolAPI;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        myApplication = MyApplication.getInstance();
        schoolAPI = APIClient.getClient().create(SchoolAPI.class);
        myApplication.getBus().register(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.fragment_school, container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        title = view.findViewById(R.id.txt_title);
        title.setText(getResources().getString(R.string.app_name));
        mProgressBar = view.findViewById(R.id.progressBar);
        recyclerView = view.findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this.getActivity()));
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (myApplication == null || myApplication.getSchools() == null || myApplication.getSchools().size() == 0) {
            setSchoolData();
        } else {
            setAdapter();
        }
    }

    /**
     * This setAdapter method set adapter to the
     * RecyclerView
     */
    protected void setAdapter() {
        SchoolAdapter adapter = new SchoolAdapter(myApplication.getSchools());
        if (recyclerView != null) {
            recyclerView.setAdapter(adapter);
        }
    }

    /**
     * Make a network request to the School list API
     * After getting successful response set School list to the MyApplication class
     */
    protected void setSchoolData() {
        if (myApplication.getNetworkUtility().isNetworkAvailable(this.getActivity())) {
            if (mProgressBar != null) {
                mProgressBar.setVisibility(View.VISIBLE);
            }
            schoolAPI = APIClient.getClient().create(SchoolAPI.class);
            Call<List<School>> schoolCall = schoolAPI.getSchools();
            schoolCall.enqueue(new Callback<List<School>>() {
                @Override
                public void onResponse(Call<List<School>> call, Response<List<School>> response) {
                    myApplication.setSchools(response.body());
                    if (mProgressBar != null) {
                        mProgressBar.setVisibility(View.GONE);
                    }
                    setAdapter();
                }

                @Override
                public void onFailure(Call<List<School>> call, Throwable t) {
                    mProgressBar.setVisibility(View.GONE);
                }
            });
        } else {
            Utility.alertOK(this.getActivity(), "Network Unavailable", false);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        myApplication.getBus().unregister(this);
    }
}