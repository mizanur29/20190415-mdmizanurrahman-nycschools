package com.nycschools;

import android.app.Application;

import com.nycschools.model.School;
import com.nycschools.model.Score;
import com.nycschools.utility.FragmentController;
import com.nycschools.utility.NetworkUtility;
import com.squareup.otto.Bus;

import java.util.List;

public class MyApplication extends Application {

    private static MyApplication application;
    private Bus bus;
    private FragmentController fragmentController;
    private NetworkUtility networkUtility;
    private List<School> schools;
    private List<Score> scores;

    public static MyApplication getInstance() {
        if (application == null) {
            application = new MyApplication();
        }
        return application;
    }

    public Bus getBus(){
        if(bus == null){
            bus = new Bus();
        }
        return bus;
    }

    public FragmentController getFragmentController(){
        if(fragmentController == null){
            fragmentController = new FragmentController();
        }
        return fragmentController;
    }

    public NetworkUtility getNetworkUtility(){
        if(networkUtility == null){
            networkUtility = new NetworkUtility();
        }
        return networkUtility;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        getInstance();
    }

    public List<School> getSchools() {
        return schools;
    }

    public void setSchools(List<School> schools) {
        this.schools = schools;
    }

    public List<Score> getScores() {
        return scores;
    }

    public void setScores(List<Score> scores) {
        this.scores = scores;
    }
}