package com.nycschools.bus;

import com.nycschools.model.School;

public class SchoolSelectEvent {
    private School school;

    public SchoolSelectEvent(School school) {
        this.school = school;
    }

    public School getSchool() {
        return school;
    }
}
