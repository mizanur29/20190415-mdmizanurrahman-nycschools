package com.nycschools.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nycschools.MyApplication;
import com.nycschools.R;
import com.nycschools.bus.SchoolSelectEvent;
import com.nycschools.model.School;

import java.util.List;

/**
 * A simple {@link RecyclerView.Adapter} subclass.
 * create an instance of this SchoolAdapter.
 */
public class SchoolAdapter extends RecyclerView.Adapter<SchoolAdapter.ViewHolder> {

    private List<School> schools;

    /**
     * Use this factory method to create a new instance of
     * this adapter using the provided parameters.
     * @param schools Parameter 1.
     * @return A new instance of SchoolAdapter.
     */
    public SchoolAdapter(List<School> schools) {
        this.schools = schools;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.school_list_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.textView.setText(schools.get(position).getName());
        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyApplication.getInstance().getBus().post(new SchoolSelectEvent(schools.get(position)));
            }
        });
    }

    @Override
    public int getItemCount() {
        return schools.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView textView;
        public LinearLayout layout;

        public ViewHolder(View itemView) {
            super(itemView);
            this.textView = (TextView) itemView.findViewById(R.id.textView);
            this.layout = itemView.findViewById(R.id.item_layout);
        }
    }

}
