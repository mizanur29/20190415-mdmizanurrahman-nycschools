package com.nycschools.network.services;

import com.nycschools.model.School;
import com.nycschools.model.Score;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;

public interface SchoolAPI {

    @Headers("Content-Type: application/json")
    @GET("s3k6-pzi2.json")
    Call<List<School>> getSchools();

    @Headers("Content-Type: application/json")
    @GET("f9bf-2cp4.json")
    Call<List<Score>> getScores();
}
