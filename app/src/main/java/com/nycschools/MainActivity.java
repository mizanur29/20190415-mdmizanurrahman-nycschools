package com.nycschools;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.nycschools.bus.SchoolSelectEvent;
import com.nycschools.ui.SchoolDetailsFragment;
import com.nycschools.ui.SchoolFragment;
import com.nycschools.utility.FragmentController;
import com.squareup.otto.Subscribe;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = MainActivity.class.getSimpleName();
    private MyApplication myApplication;
    private FragmentController fragmentController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        myApplication = MyApplication.getInstance();
        fragmentController = MyApplication.getInstance().getFragmentController();
        myApplication.getBus().register(this);

        fragmentController.changeFragment(new SchoolFragment(), R.id.container, getSupportFragmentManager());
    }

    @Subscribe
    public void onSchoolSelected(SchoolSelectEvent schoolSelectEvent) {
        fragmentController.changeFragment(SchoolDetailsFragment.newInstance(schoolSelectEvent.getSchool()), R.id.container, getSupportFragmentManager());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        myApplication.getBus().unregister(this);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (getSupportFragmentManager().getBackStackEntryCount() == 0) {
            finish();
        }
    }
}
