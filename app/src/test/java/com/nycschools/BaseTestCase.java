package com.nycschools;

import android.support.v4.app.FragmentActivity;

import com.nycschools.model.School;
import com.nycschools.model.Score;

import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import java.util.ArrayList;
import java.util.List;

@Config(sdk = 28, manifest=Config.NONE)
@RunWith(RobolectricTestRunner.class)
public abstract class BaseTestCase {

    private FragmentActivity activity;

    @Before
    public void setUp() {
        initialize();
    }

    @After
    public void tearDown() {

    }

    protected FragmentActivity getActivity() {
        if (activity == null) {
            activity = Robolectric.setupActivity(FragmentActivity.class);
        }

        return activity;
    }

    private void initialize() {
        List<School> schools = new ArrayList<>();
        School school = new School();
        school.setSchoolDBN("M2");
        school.setName("Test");
        school.setEmail("email");
        school.setAddress("address");
        school.setCity("city");
        school.setZipCode("12345");
        schools.add(school);

        List<Score> scores = new ArrayList<>();
        Score score = new Score();
        score.setSchoolDBN("M2");
        score.setSchoolName("Test");
        score.setMathScore("1");
        score.setNumOfApplicant("1");
        score.setReadingScore("1");
        score.setWritingScore("1");
        scores.add(score);
        MyApplication.getInstance().setSchools(schools);
        MyApplication.getInstance().setScores(scores);
    }
}