package com.nycschools;

import org.junit.Before;
import org.junit.Test;
import static junit.framework.Assert.assertNotNull;

public class MyApplicationTest extends BaseTestCase{
     private MyApplication subject;
    @Before
    public void setUp() {
        subject = MyApplication.getInstance();
    }

    @Test
    public void check_get_bus_return_not_null(){
        assertNotNull(subject.getBus());
    }

    @Test
    public void check_get_fragmentController_return_not_null(){
        assertNotNull(subject.getFragmentController());
    }
}
