package com.nycschools;

import com.nycschools.bus.SchoolSelectEvent;
import com.nycschools.ui.SchoolDetailsFragment;
import com.nycschools.ui.SchoolFragment;

import org.junit.Before;
import org.junit.Test;
import org.mockito.MockitoAnnotations;
import org.robolectric.Robolectric;
import org.robolectric.android.controller.ActivityController;

import static junit.framework.Assert.assertEquals;

public class MainActivityTest extends BaseTestCase {

    private ActivityController<MainActivity> activityController;
    private MainActivity subject;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        activityController = Robolectric.buildActivity(MainActivity.class);
        subject = activityController.get();
        activityController.create();
    }

    @Test
    public void whenActivityIsCreatedThenMainViewIsSchoolListPage(){
        assertEquals(SchoolFragment.class, subject.getSupportFragmentManager().findFragmentById(R.id.container).getClass());
    }

    @Test
    public void whenSchoolSelectedThenMainViewIsSchoolDetailsPage(){
        MyApplication.getInstance().getBus().post(new SchoolSelectEvent(null));
        assertEquals(SchoolDetailsFragment.class, subject.getSupportFragmentManager().findFragmentById(R.id.container).getClass());
    }
}
