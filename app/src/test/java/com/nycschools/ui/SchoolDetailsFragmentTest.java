package com.nycschools.ui;

import android.app.AlertDialog;
import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.nycschools.BaseTestCase;
import com.nycschools.MyApplication;
import com.nycschools.R;
import com.nycschools.network.services.SchoolAPI;
import com.nycschools.utility.NetworkUtility;

import org.junit.Before;
import org.junit.Test;
import org.robolectric.shadows.ShadowAlertDialog;
import org.robolectric.shadows.support.v4.SupportFragmentController;

import retrofit2.Call;

import static junit.framework.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;
import static org.robolectric.Shadows.shadowOf;

public class SchoolDetailsFragmentTest extends BaseTestCase {

    private SchoolDetailsFragment subject;

    @Before
    public void setUp() {
        super.setUp();

        subject = SchoolDetailsFragment.newInstance(MyApplication.getInstance().getSchools().get(0));
        SupportFragmentController.setupFragment(subject);
    }

    @Test
    public void check_getSchool_return_school_data() {
        assertNotNull(subject.getSchool());
    }

    @Test
    public void when_view_created_then_all_view_components_are_visible() {
        assertEquals(View.VISIBLE, subject.getView().findViewById(R.id.txt_title).getVisibility());
        assertEquals(View.VISIBLE, subject.getView().findViewById(R.id.layout_back_button).getVisibility());
        assertEquals(View.VISIBLE, subject.getView().findViewById(R.id.txt_dbn).getVisibility());
        assertEquals(View.VISIBLE, subject.getView().findViewById(R.id.txt_school_name).getVisibility());
        assertEquals(View.VISIBLE, subject.getView().findViewById(R.id.txt_address).getVisibility());
        assertEquals(View.VISIBLE, subject.getView().findViewById(R.id.txt_email).getVisibility());
        assertEquals(View.VISIBLE, subject.getView().findViewById(R.id.txt_reading_score).getVisibility());
        assertEquals(View.VISIBLE, subject.getView().findViewById(R.id.txt_math_score).getVisibility());
        assertEquals(View.VISIBLE, subject.getView().findViewById(R.id.txt_writing_score).getVisibility());
        assertEquals(View.GONE, subject.getView().findViewById(R.id.progressBar).getVisibility());
        assertEquals(View.VISIBLE, subject.getView().findViewById(R.id.layout_sat_details).getVisibility());
    }

    @Test
    public void check_action_bar_left_button_click() {
        TextView view = subject.getView().findViewById(R.id.txt_title);
        assertEquals(getActivity().getResources().getString(R.string.sat_title), view.getText());
        assertTrue(subject.getView().findViewById(R.id.layout_back_button).callOnClick());
    }

    @Test
    public void check_setScore_method_set_data_to_view_element() {
        TextView txtDBN = subject.getView().findViewById(R.id.txt_dbn);
        TextView txtSchoolName = subject.getView().findViewById(R.id.txt_school_name);
        TextView txtAddress = subject.getView().findViewById(R.id.txt_address);
        TextView txtEmail = subject.getView().findViewById(R.id.txt_email);
        TextView txtNumberOfApplicants = subject.getView().findViewById(R.id.txt_num_of_applicants);
        TextView txtReadingScore = subject.getView().findViewById(R.id.txt_reading_score);
        TextView txtMathScore = subject.getView().findViewById(R.id.txt_math_score);
        TextView txtWritingScore = subject.getView().findViewById(R.id.txt_writing_score);

        assertEquals("M2",txtDBN.getText());
        assertEquals("Test",txtSchoolName.getText());
        assertEquals("address, city, 12345",txtAddress.getText());
        assertEquals("email",txtEmail.getText());
        assertEquals("1",txtNumberOfApplicants.getText());
        assertEquals("1",txtReadingScore.getText());
        assertEquals("1",txtMathScore.getText());
        assertEquals("1",txtWritingScore.getText());
    }

    @Test
    public void when_no_network_connection_then_check_alert_dialog_was_shown() {
        subject = spy(SchoolDetailsFragment.class);
        subject.myApplication = mock(MyApplication.class);
        NetworkUtility networkUtility = mock(NetworkUtility.class);
        when(networkUtility.isNetworkAvailable(any(Context.class))).thenReturn(false);
        when(subject.myApplication.getNetworkUtility()).thenReturn(networkUtility);
        when(subject.myApplication .getScores()).thenReturn(null);
        when(subject.getActivity()).thenReturn(getActivity());
        subject.onResume();

        AlertDialog alertDialog = ShadowAlertDialog.getLatestAlertDialog();
        ShadowAlertDialog shadowAlertDialog = shadowOf(alertDialog);
        assertNotNull(shadowAlertDialog);
    }

    @Test
    public void when_network_connection_true_then_check_alert_dialog_was_shown() {
        subject = spy(SchoolDetailsFragment.class);
        subject.myApplication = mock(MyApplication.class);
        subject.schoolAPI = mock(SchoolAPI.class);
        NetworkUtility networkUtility = mock(NetworkUtility.class);
        when(networkUtility.isNetworkAvailable(any(Context.class))).thenReturn(true);
        when(subject.myApplication.getNetworkUtility()).thenReturn(networkUtility);
        when(subject.myApplication .getScores()).thenReturn(null);
        when(subject.schoolAPI.getScores()).thenReturn(mock(Call.class));
        when(subject.getActivity()).thenReturn(getActivity());
        subject.onResume();

        AlertDialog alertDialog = ShadowAlertDialog.getLatestAlertDialog();
        assertNull(alertDialog);
    }

}