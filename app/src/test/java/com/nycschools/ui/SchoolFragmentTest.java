package com.nycschools.ui;

import android.app.AlertDialog;
import android.content.Context;
import android.view.View;

import com.nycschools.BaseTestCase;
import com.nycschools.MyApplication;
import com.nycschools.R;
import com.nycschools.model.School;
import com.nycschools.utility.NetworkUtility;

import org.junit.Before;
import org.junit.Test;
import org.robolectric.shadows.ShadowAlertDialog;
import org.robolectric.shadows.support.v4.SupportFragmentController;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.robolectric.Shadows.shadowOf;

public class SchoolFragmentTest extends BaseTestCase {

    private SchoolFragment subject;

    @Before
    public void setUp() {
        subject = new SchoolFragment();
        SupportFragmentController.setupFragment(subject);
    }

    @Test
    public void when_fragment_is_created_check_view_is_not_null() {
        assertNotNull(subject.getView());
    }

    @Test
    public void when_view_created_then_all_view_components_are_visible() {
        assertEquals(View.VISIBLE, subject.getView().findViewById(R.id.txt_title).getVisibility());
        assertEquals(View.VISIBLE, subject.getView().findViewById(R.id.recyclerView).getVisibility());
        assertNotNull(subject.getView().findViewById(R.id.progressBar));
    }

    @Test
    public void verify_setSchoolData_method_is_called_when_on_resume_call_and_schools_are_null() {
        subject = spy(SchoolFragment.class);
        subject.myApplication = mock(MyApplication.class);
        NetworkUtility networkUtility = mock(NetworkUtility.class);
        when(networkUtility.isNetworkAvailable(any(Context.class))).thenReturn(false);
        when(subject.myApplication.getNetworkUtility()).thenReturn(networkUtility);
        subject.onResume();
        verify(subject).setSchoolData();
    }

    @Test
    public void verify_setAdapter_method_is_called_when_on_resume_call_and_schools_are_not_null() {
        subject = spy(SchoolFragment.class);
        List<School> schools = new ArrayList<>();
        School school = new School();
        school.setName("Test");
        schools.add(school);
        subject.onCreate(null);
        MyApplication.getInstance().setSchools(schools);
        subject.onResume();
        verify(subject).setAdapter();
    }

    @Test
    public void when_no_network_connection_then_check_alert_dialog_was_shown() {
        subject = spy(SchoolFragment.class);
        subject.myApplication = mock(MyApplication.class);
        NetworkUtility networkUtility = mock(NetworkUtility.class);
        when(networkUtility.isNetworkAvailable(any(Context.class))).thenReturn(false);
        when(subject.myApplication.getNetworkUtility()).thenReturn(networkUtility);
        when(subject.getActivity()).thenReturn(getActivity());
        subject.onResume();

        AlertDialog alertDialog = ShadowAlertDialog.getLatestAlertDialog();
        ShadowAlertDialog shadowAlertDialog = shadowOf(alertDialog);
        assertNotNull(shadowAlertDialog);
    }

    @Test
    public void when_network_connection_true_then_check_alert_dialog_was_shown() {
        subject = spy(SchoolFragment.class);
        subject.myApplication = mock(MyApplication.class);
        NetworkUtility networkUtility = mock(NetworkUtility.class);
        when(networkUtility.isNetworkAvailable(any(Context.class))).thenReturn(true);
        when(subject.myApplication.getNetworkUtility()).thenReturn(networkUtility);
        when(subject.getActivity()).thenReturn(getActivity());
        subject.onResume();

        AlertDialog alertDialog = ShadowAlertDialog.getLatestAlertDialog();
        assertNull(alertDialog);
    }
}