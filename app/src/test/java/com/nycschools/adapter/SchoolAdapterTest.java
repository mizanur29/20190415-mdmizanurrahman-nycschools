package com.nycschools.adapter;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nycschools.BaseTestCase;
import com.nycschools.model.School;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;


public class SchoolAdapterTest extends BaseTestCase {

    private SchoolAdapter subject;
    @Before
    public void setUp() {
        List<School> schools = new ArrayList<>();
        School school = new School();
        school.setName("Test");
        schools.add(school);
        subject = new SchoolAdapter(schools);
    }

    @Test
    public void should_getItem_method_return_school_list(){
        assertEquals(1, subject.getItemCount());
    }

    @Test
    public void verify_obBindViewHolder_having_text_view_and_layout(){
        SchoolAdapter.ViewHolder viewHolder = mock(SchoolAdapter.ViewHolder.class);
        viewHolder.textView = mock(TextView.class);
        viewHolder.layout = mock(LinearLayout.class);
        subject.onBindViewHolder(viewHolder, 0);
        verify(viewHolder.textView).setText(any(String.class));
        verify(viewHolder.layout).setOnClickListener(any(View.OnClickListener.class));
    }
}
