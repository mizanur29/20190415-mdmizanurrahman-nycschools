# NYC Schools

A small android application for NYC schools to show school list and SAT results.

## Libraries 

- [Retrofit](https://square.github.io/retrofit/) for network request
- [Mockito](https://site.mockito.org/) to mock data for unit test
- [Robolectric](http://robolectric.org/) for unit test
- [Otto](https://square.github.io/otto/) An event bus to communicate between Activity and different Fragments